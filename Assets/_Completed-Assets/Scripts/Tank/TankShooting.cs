﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Mirror;

namespace Complete
{
    public class TankShooting : NetworkBehaviour
    {
        public int m_PlayerNumber = 1;              // Usado para identificar a los jugadores.
        public GameObject m_Bullet;                 // Prefab de la bala normal.
        public GameObject m_FastBullet;             // Prefab de la bala rapida.
        public Transform m_FireTransform;           // Punto de spawn de las balas, hijo del tanque.
        public AudioSource m_ShootingAudio;         // Referencia al clip de audio usado en para el sonido de los disparos.
        public AudioClip m_FireClip;                // El clip de audio usado para los disparos.

        private InputAction m_FireAction;           // Referencia de la accion 'Disparo'        (Unity 2020 New Input System) 
        private InputAction m_FastFireAction;       // Referencia de la accion 'Disparo Rapido' (Unity 2020 New Input System) 
        private bool isDisabled = false;            // Para evitar habilitar/deshabilitar el Input System cuando el tanque es destruido
        private bool canFire = true;                // Indica si puede disparar la IA

        private void OnEnable()
        {
            isDisabled = false;
        }

        private void OnDisable()
        {
            isDisabled = true;
        }

        private void Start()
        {
            // Unity 2020 New Input System
            // Referencia al EventSystem para este jugador.
            EventSystem ev = GameObject.Find("EventSystem").GetComponent<EventSystem>();

            // Encontrar el Mapa de Acciones del Tanque y habilitarlo.
            InputActionMap playerActionMap = ev.GetComponent<PlayerInput>().actions.FindActionMap("Tank");
            playerActionMap.Enable();

            // Accion de 'Disparo'.
            m_FireAction = playerActionMap.FindAction("Fire");
            m_FireAction.Enable();
            m_FireAction.performed += OnFire;

            // Accion de 'Disparo Rapido'.
            m_FastFireAction = playerActionMap.FindAction("FastFire");
            m_FastFireAction.Enable();
            m_FastFireAction.performed += OnFastFire;
        }


        /// <summary>
        /// Evento de 'Disparo' llamado mediante el New Input System
        /// </summary>
        public void OnFire(InputAction.CallbackContext obj)
        {
            if (!isLocalPlayer)
                return;

            if (!isDisabled)
            {
                // Cuando el valor leido del boton es mayor que el punto de pulsacion, este boton ha sido pulsado.
                if (obj.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
                {
                    Fire(false);
                }
            }
        }

        /// <summary>
        /// Evento de 'Disparo Rapido' llamado mediante el New Input System
        /// </summary>
        public void OnFastFire(InputAction.CallbackContext obj)
        {
            if (!isLocalPlayer)
                return;

            if (!isDisabled)
            {
                // Cuando el valor leido del boton es mayor que el punto de pulsacion, este boton ha sido pulsado.
                if (obj.ReadValue<float>() >= InputSystem.settings.defaultButtonPressPoint)
                {
                    Fire(true);
                }
            }
        }

        public void Fire(bool isFast)
        {
            CmdFire(isFast);

            // Cambiar al clip de audio de disparo y hacerlo sonar
            m_ShootingAudio.clip = m_FireClip;
            m_ShootingAudio.Play();
        }

        /// <summary>
        /// Instanciacion de la bala en servidor, llamado desde el jugador
        /// </summary>
        [Command]
        void CmdFire(bool isFast)
        {
            if (isFast)
            {
                var bullet = (GameObject)Instantiate(m_FastBullet, m_FireTransform.position, m_FireTransform.rotation);
                NetworkServer.Spawn(bullet);
            }
            else
            {
                var bullet = (GameObject)Instantiate(m_Bullet, m_FireTransform.position, m_FireTransform.rotation);
                NetworkServer.Spawn(bullet);
            }
        }

        /// <summary>
        /// Este metodo sera llamado por la IA desde servidor para ejecutar su disparo.
        /// </summary>
        [Client]
        public void FireAI()
        {
            if (canFire)
            {
                CmdFireAI();

                // Cambiar al clip de audio de disparo y hacerlo sonar
                m_ShootingAudio.clip = m_FireClip;
                m_ShootingAudio.Play();

                canFire = false;
            }

            Invoke("activarDisparo", 1.2f);
        }

        /// <summary>
        /// Activa el disparo de la IA.
        /// </summary>
        public void activarDisparo()
        {
            canFire = true;
            CancelInvoke("activarDisparo");
        }

        /// <summary>
        /// Instanciacion de la bala en servidor, llamado desde la IA
        /// </summary>
        [Command(requiresAuthority = false)]
        void CmdFireAI(NetworkConnectionToClient sender = null)
        {
            var bullet = (GameObject)Instantiate(m_Bullet, m_FireTransform.position, m_FireTransform.rotation);
            NetworkServer.Spawn(bullet);
        }

    }
}