﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using System.Collections.Generic;
using Complete;
using UnityEngine.AI;

namespace BBUnity.Actions
{
    /// <summary>
    /// Se mueve entre los waypoints asignados
    /// </summary>
    [Action("Navigation/MoverseWayPoints")]
    [Help("Se mueve entre los waypoints asignados")]
    public class MoverseWayPoints : GOAction
    {

        /// <summary>
        /// Ejecucion movimiento
        /// </summary>
        public override void OnStart()
        {
            //Agente de navmesh para controlar la entidad
            NavMeshAgent tanke = this.gameObject.GetComponent<NavMeshAgent>();

            //Para obtener posiciones y controlar cosas del movimiento
            TankMovement movimiento = this.gameObject.GetComponent<TankMovement>();

            //Obtenemos todas las posiciones
            Transform[] posiciones = movimiento.waypoints.ToArray();
            int posicion = movimiento.posicionWaypoints;

            //Calculamos del destino unos maximos y minimos para evitar que el enemigo no llegue a la zona por obstaculos
            float minX, maxX, minZ, maxZ;
            minX = posiciones[posicion].position.x - 3f;
            maxX = posiciones[posicion].position.x + 3f;
            minZ = posiciones[posicion].position.z - 3f;
            maxZ = posiciones[posicion].position.z + 3f;

            //comprobamos si llego al destino
            if (maxX >= this.gameObject.transform.position.x
            && minX <= this.gameObject.transform.position.x
            && maxZ >= this.gameObject.transform.position.z
            && minZ <= this.gameObject.transform.position.z)
            {
                //Ha llegado, cambiamos posicion
                posicion++;
                if (!(posicion < posiciones.Length))
                {
                    //posicion no encontrada reiniciamos
                    posicion = 0;
                }
                //Actualizamos posicion en el objeto
                movimiento.posicionWaypoints = posicion;
            }

            //actualizamos objetivo
            tanke.destination = posiciones[posicion].position;
            tanke.speed = 6;  
        }

        /// <summary>
        /// Devolucion movimiento
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
                return TaskStatus.COMPLETED;
        }
    }
}