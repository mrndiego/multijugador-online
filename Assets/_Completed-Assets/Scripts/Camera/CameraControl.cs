﻿using UnityEngine;

namespace Complete
{
    /// <summary>
    /// Clase que se ocupa de gestionar la camara
    /// </summary>
    public class CameraControl : MonoBehaviour
    {
        public float m_DampTime = 0.2f;                 // Tiempo aproximado para que la camara vuelva a enfocar.
        public float m_ScreenEdgeBuffer = 4f;           // Espacio entre los objetivos superior e inferior y el borde de la pantalla.
        public float m_MinSize = 6.5f;                  // El tamaño ortográfico más pequeño que puede tener la camara.
        [HideInInspector] public Transform[] m_Targets;  // Todos los objetivos que la camara necesita cubrir.


        private Camera m_Camera;                        // Referencia a la camara.
        private float m_ZoomSpeed;                      // Velocidad de referencia para el ajuste fluido del tamaño ortografico.
        private Vector3 m_MoveVelocity;                 // Velocidad de referencia para el ajuste fluido de la posicion.
        private Vector3 m_DesiredPosition;              // La posicion a la que se esta moviendo la camara.


        private void Awake()
        {
            m_Camera = GetComponentInChildren<Camera>();
        }

        /// <summary>
        /// Se ocupa de mover la camara y realizar el zoom correcto.
        /// </summary>
        private void FixedUpdate()
        {
            Move();
            Zoom();
        }

        /// <summary>
        /// Mueve la camara.
        /// </summary>
        private void Move()
        {
            // Encuentra la posicion media de los objetivos.
            FindAveragePosition();

            // Transicion fluida a esa posicion.
            transform.position = Vector3.SmoothDamp(transform.position, m_DesiredPosition, ref m_MoveVelocity, m_DampTime);
        }

        /// <summary>
        /// Calcula la media de las posiciones de los jugadores y enemigos para mover la camara.
        /// </summary>
        private void FindAveragePosition()
        {
            GameObject[] jugadoresRestantes, enemigosRestantes, PlayersLeft;
            jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");
            enemigosRestantes = GameObject.FindGameObjectsWithTag("Enemy");

            // Fusionamos los arrays de jugadores y enemigos.
            PlayersLeft = new GameObject[jugadoresRestantes.Length + enemigosRestantes.Length];
            jugadoresRestantes.CopyTo(PlayersLeft, 0);
            enemigosRestantes.CopyTo(PlayersLeft, jugadoresRestantes.Length);


            Vector3 averagePos = new Vector3();
            int numTargets = 0;

            // Revisa todos los objetivos y suma sus posiciones.
            for (int i = 0; i < PlayersLeft.Length; i++)
            {
                // Saltarse los objetivos inactivos.
                if (!PlayersLeft[i].activeSelf)
                {
                    continue;
                }

                // Suma para la media e incrementa el numero de objetivos en esta.
                averagePos += PlayersLeft[i].GetComponent<Transform>().position;
                numTargets++;
            }

            // Si hay objetivos, divide la suma de las posiciones por el numero de ellos para encontrar la media.
            if (numTargets > 0)
            {
                averagePos /= numTargets;
            }

            // Mantener el valor y.
            averagePos.y = transform.position.y;

            // La posicion deseada es la media.
            m_DesiredPosition = averagePos;
        }

        /// <summary>
        /// Obtiene el zoom correcto y lo aplica.
        /// </summary>
        private void Zoom()
        {
            // Encuentra el tamaño requerido en funcion de la posicion deseada y hace una transición fluida a ese tamaño.
            float requiredSize = FindRequiredSize();
            m_Camera.orthographicSize = Mathf.SmoothDamp(m_Camera.orthographicSize, requiredSize, ref m_ZoomSpeed, m_DampTime);
        }

        /// <summary>
        /// Calcula el zoom.
        /// </summary>
        /// <returns>Tamaño del zoom</returns>
        private float FindRequiredSize()
        {
            GameObject[] jugadoresRestantes, enemigosRestantes, PlayersLeft;
            jugadoresRestantes = GameObject.FindGameObjectsWithTag("Player");
            enemigosRestantes = GameObject.FindGameObjectsWithTag("Enemy");

            // Fusionamos los arrays de jugadores y enemigos.
            PlayersLeft = new GameObject[jugadoresRestantes.Length + enemigosRestantes.Length];
            jugadoresRestantes.CopyTo(PlayersLeft, 0);
            enemigosRestantes.CopyTo(PlayersLeft, jugadoresRestantes.Length);

            // Encuentra la posicion hacia la que se mueve la camara en su espacio local.
            Vector3 desiredLocalPos = transform.InverseTransformPoint(m_DesiredPosition);

            // Comienza el calculo del tamaño de la camara desde cero.
            float size = 0f;

            // Go through all the targets...
            for (int i = 0; i < PlayersLeft.Length; i++)
            {
                // Saltarse los objetivos inactivos.
                if (!PlayersLeft[i].activeSelf)
                {
                    continue;
                }

                // Encuentra la posicion del objetivo en el espacio local de la camara.
                Vector3 targetLocalPos = transform.InverseTransformPoint(PlayersLeft[i].GetComponent<Transform>().position);

                // Find the position of the target from the desired position of the camera's local space
                Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

                // Escoger el tamaño mas grande entre tamaño actual y el tamaño calculado en función del tanque que este encima o abajo de la cámara
                size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));

                // Escoger el tamaño mas grande entre tamaño actual y el tamaño calculado en función del tanque que este a la izquierda o a la derecha de la cámara
                size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / m_Camera.aspect);
            }

            // Añadir margen al tamaño.
            size += m_ScreenEdgeBuffer;

            // Asegurarse que el tamaño de camara no es menor al minimo.
            size = Mathf.Max(size, m_MinSize);

            return size;
        }

        /// <summary>
        /// Posicion y tamaño inicial.
        /// </summary>
        public void SetStartPositionAndSize()
        {
            // Encuentra la posicion media de los objetivos.
            FindAveragePosition();

            // Cambio de posicion de camara immediato.
            transform.position = m_DesiredPosition;

            // Cambio de tamaño de camara immediato.
            m_Camera.orthographicSize = FindRequiredSize();
        }
    }
}