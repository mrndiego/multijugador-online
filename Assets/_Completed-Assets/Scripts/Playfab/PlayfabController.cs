using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayfabController : MonoBehaviour
{
    public static PlayfabController PFC;

    private string userEmail;                   // User's e-mail address
    private string userPassword;                // User's password
    private string username;                    // User's name
    public GameObject loginPanel;               // Login panel reference

    private void OnEnable()
    {
        if (PlayfabController.PFC == null)
        {
            PlayfabController.PFC = this;
        }
        else
        {
            if (PlayfabController.PFC != this)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    // Start is called before the first frame update
    public void Start()
    {
        // Note: Setting title Id here can be skipped if you have set the value in Editor Extensions already.
        if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
        {
            PlayFabSettings.staticSettings.TitleId = "C819A"; // Please change this value to your own titleId from PlayFab Game Manager
        }

        if (PlayerPrefs.HasKey("Email"))
        {
			//We get email and password from previous login.
            userEmail = PlayerPrefs.GetString("Email");
            userPassword = PlayerPrefs.GetString("Password");

            var request = new LoginWithEmailAddressRequest { Email = userEmail, Password = userPassword };
            PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginFailure);

            Debug.Log(userEmail + " - Autologin");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("Congratulations, you made your first successful API call!");
        GoToLobby();
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your first API call.");
        Debug.LogError("Here?s some debug information:");
        Debug.LogError(error.GenerateErrorReport());
        ModalError.mostrarError("Usuario no valido","No se encontro el usuario, vamos a proceder a darlo de alta.", 6);
        var registerRequest = new RegisterPlayFabUserRequest { Email = userEmail, Password = userPassword, Username = username, DisplayName=username };
        PlayFabClientAPI.RegisterPlayFabUser(registerRequest, OnRegisterSuccess, OnRegisterFailure);
    }

    // Method invoked on user registration failure
    private void OnRegisterFailure(PlayFabError error)
    {
        Debug.LogError(error.GenerateErrorReport());
        ModalError.mostrarError("Error en registro", "Ocurrio el siguiente error: "+error.GenerateErrorReport(), 8);
    }

    // Method invoked when new user registration is successful
    private void OnRegisterSuccess(RegisterPlayFabUserResult result)
    {
        GoToLobby();
    }

    private void GoToLobby()
    {
		//Guarda en Player Prefs para habilitar el autologin
        PlayerPrefs.SetString("Email", userEmail);
        PlayerPrefs.SetString("Password", userPassword);
        //Obtener stats
        GetDisplayName();
       
        //cambiar de escena
        SceneManager.LoadScene("Menu");
    }

    // Modifica el email del usuario
    public void SetUserEmail(string emailIn)
    {
        userEmail = emailIn;
    }

    // Modifica la contrase�a del usuario
    public void SetUserPassword(string passwordIn)
    {
        userPassword = passwordIn;
    }

    // Modifica el nombre de usuario
    public void SetUserName(string usernameIn)
    {
        username = usernameIn;
    }

    // Se ejecuta cuando se pulsa el bot�n de login
    public void OnClickLogin()
    {
        var request = new LoginWithEmailAddressRequest { Email = userEmail, Password = userPassword };
        PlayFabClientAPI.LoginWithEmailAddress(request, OnLoginSuccess, OnLoginFailure);
    }

	public void OnClickLogout()
	{
		PlayFabClientAPI.ForgetAllCredentials();
		PlayerPrefs.DeleteAll();
		SceneManager.LoadScene("Login");
	}

    #region PlayerStats

    public string playerDisplayName;                 // Player's level statistic


    // Set player statistics via client (this method will only work if the "Allow client to post player statistics" is checked
    // on PlayFab Dashboard > Settings > API Features

   public void GetDisplayName()
    {
        PlayFabClientAPI.GetPlayerProfile(new GetPlayerProfileRequest(),
        result => {
            if (result.PlayerProfile.DisplayName == null)
            {
                Debug.Log("No PlayerDisplayName");
            }
            else
            {
                playerDisplayName = result.PlayerProfile.DisplayName;
                Debug.Log("PlayerDisplayName: " + playerDisplayName);
                PlayerPrefs.SetString("DisplayName", playerDisplayName);
                Debug.Log(PlayerPrefs.GetString("DisplayName"));
                DisplayName.PlayerDisplayName = playerDisplayName;
                Debug.Log(DisplayName.PlayerDisplayName);
            }
        },
       error => {
           ModalError.mostrarError("Error de transferencia", "No se pudo obtener TitleData.", 5);
           Debug.Log("Got error getting titleData:");
           Debug.Log(error.GenerateErrorReport());
       }
   );
    }

    // Method to get player statistics
    public void GetStats()
    {
        //PlayFabClientAPI.GetPlayerStatistics(new GetPlayerStatisticsRequest(), OnGetStatistics, error => Debug.LogError(error.GenerateErrorReport()));
       PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(),
       result => {
           if (result.Data == null || !result.Data.ContainsKey("PlayerDisplayName"))
           {
               Debug.Log("No PlayerDisplayName");
               
           }
           else
           {
               Debug.Log("PlayerDisplayName: " + result.Data["PlayerDisplayName"]);
               playerDisplayName = result.Data["PlayerDisplayName"];
           }
          
       
       },
       error => {
           ModalError.mostrarError("Error de transferencia", "No se pudo obtener TitleData.", 5);
           Debug.Log("Got error getting titleData:");
           Debug.Log(error.GenerateErrorReport());
       }
   );
    }

    // Method invoked when player statistics are received
   /* public void OnGetStatistics(GetPlayerStatisticsResult result)
    {
        Debug.Log("Received the following Statistics:");
        foreach (var eachStat in result.Statistics)
        {
            Debug.Log("Statistic (" + eachStat.StatisticName + "): " + eachStat.Value);
            switch (eachStat.StatisticName)
            {
                case "PlayerDisplayName":
                    playerDisplayName = eachStat.Value;
                    break;
            }
        }
    }*/
    #endregion PlayerStats

}
