using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class LobbyManager : MonoBehaviour
{
    public TextMeshProUGUI InfoText;
    // Start is called before the first frame update
    void Start()
    {
        InfoText.text = "Welcome, "+PlayerPrefs.GetString("DisplayName")+"\n"+ PlayerPrefs.GetString("Email");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
