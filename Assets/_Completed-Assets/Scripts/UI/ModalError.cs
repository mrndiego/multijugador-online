using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ModalError : MonoBehaviour
{
    /// <summary>
    /// Muestra el error por un periodo de tiempo
    /// </summary>
    public static Action<String, String, int> mostrarError;
    
    /// <summary>
    /// Cierra el error
    /// </summary>
    public static Action closeError;

    /// <summary>
    /// Contiene el panel del error
    /// </summary>
    public GameObject panelError;

    /// <summary>
    /// Textos de message y title
    /// </summary>
    public TextMeshProUGUI message,title;

    // Start is called before the first frame update
    void Start()
    {
        //Definimos el evento global
        mostrarError = verError;
        closeError = cerrarError;

        //Si el panel no es nulo, lo ocultamos
        if(panelError != null)
        {
            panelError.SetActive(false);
        }
    }

    /// <summary>
    /// Muestra el error por un periodo de tiempo
    /// </summary>
    private void verError(String title, String message, int time)
    {
        CancelInvoke("cerrarError");
        panelError.SetActive(true);
        this.message.text = message;
        this.title.text = title;
        Invoke("cerrarError", time);
    }

    /// <summary>
    /// Cierra la modal de error
    /// </summary>
    private void cerrarError()
    {
        panelError.SetActive(false);
    }
}
