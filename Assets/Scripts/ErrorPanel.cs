using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrorPanel : MonoBehaviour
{
    
    float timeShown = 0;
    float MaxTime = 5;

    public Text errorText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeShown += Time.deltaTime;
        if(timeShown > MaxTime)
        {
            Destroy(gameObject);
        }
    }

    public void showMessage(string error)
    {
        errorText.text = error;
        timeShown = 0;
    }

}
